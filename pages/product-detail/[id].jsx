import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from './product-detail.module.scss';
import axios from "axios";

export async function getServerSideProps(context) {
  const id = context?.params?.id || '';
  const response = await axios.get(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
  );
  const data = response?.data || {};
  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  const features = data?.details?.features || []
  const attributes = features?.length > 0 && features[0].attributes || []
  const includedServices = data?.additionalServices?.includedServices || []
  const summary = (
    <>
      <h2 className={styles.price}>{`£${data?.price?.now}`}</h2>
      <div className={styles.offer}>{data?.displaySpecialOffer}</div>
      {includedServices.map((service)=> 
        <div key={service}>{service}</div>
      )}
    </>
  )
  
  return (
    <div>
      <div className={styles.titleContainer}>
        <span>{'<'}</span>
        <h2 className={styles.title}>{data?.title}</h2>
      </div>
      <div className={styles.detailsContainer}>
        <div className={styles.productDetails}>
          <div>
            <ProductCarousel images={data?.media?.images?.urls} imageAlt={data?.title}/>
            <div className={styles.bottomSummary}>
              {summary}
            </div>
          </div>
          <div className={styles.bottomSection}>
            <div>
              <h2 className={styles.productInfo}>Product information</h2>
              <div dangerouslySetInnerHTML={{__html: data?.details?.productInformation}}></div>
              <h3>{`Product code: ${data?.productId}`}</h3>
            </div>
            <h2>Product specification</h2>
            <ul>
              {attributes.map((item) => (
                <li key={item?.name} className={styles.attributeListItem}>
                  <span>{item?.name}</span>
                  <span>{item?.value}</span>
                </li>
              ))}
            </ul>
          </div>
        </div>
        <div>
          <div className={styles.sideSummary}>
            {summary}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
