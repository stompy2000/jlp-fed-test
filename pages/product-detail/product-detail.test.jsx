import { screen, render } from "@testing-library/react";
import ProductDetail, { getServerSideProps } from "./[id]";
import mockAxios from "jest-mock-axios";
import mockProductDetailsData from "../../mockData/productDetailsData.json";

const productDetailsApiPath = 'https://api.johnlewis.com/mobile-apps/api/v1/products/'
const productCode = 123456
const defaultData = {
  displaySpecialOffer: "SpecialOffer",
  productId: productCode,
  details: {
    productInformation: 'mock description'
  },
  additionalServices: {
    includedServices: ["Included Service 1", "Included Service 2"],
  },
  title: "mockTitle",
  price: {
    now: "5.00",
  },
  media: {
    images: {
      urls: ["mockproducturl"],
    },
  },
};

beforeEach(() => {
  jest.resetAllMocks()  
})

describe("no props", () => {
  test("should render when props are undefined", () => {
    render(<ProductDetail />);
  });
});

describe("getServerSideProps", () => {
  const mockProductId = 123456
  const context = {
    params: {
      id: mockProductId
    }
  }
  test("should pass product details api data to server side props", async () => {
    mockAxios.get.mockResolvedValueOnce({data: mockProductDetailsData});
    const props = await getServerSideProps(context);
    expect(props).toEqual({ props: { data: mockProductDetailsData } });
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(`${productDetailsApiPath}${mockProductId}`);
  });
  test("should handle api error with empty data object", async () => {
    const errorResponse = {
      data: {},
      status: 500,
      statusText: 'Server Error',
      headers: {},
      config: {},
    }
    mockAxios.get.mockResolvedValueOnce(errorResponse);
    const props = await getServerSideProps(context);
    expect(props).toEqual({ props: { data: {} }});
  });
  test("should handle empty context object with no productId", async () => {
    mockAxios.get.mockResolvedValueOnce({data: mockProductDetailsData});
    const props = await getServerSideProps();
    expect(props).toEqual({ props: { data: mockProductDetailsData } });
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(`${productDetailsApiPath}`);
  });
});

describe("Product Information", () => {
  test("should render product details", () => {
    render(<ProductDetail data={defaultData} />);
    expect(screen.getByText("mockTitle")).toBeInTheDocument();
    expect(screen.getAllByText("£5.00")[0]).toBeInTheDocument();
    expect(screen.getAllByText("SpecialOffer")[0]).toBeInTheDocument();
    expect(screen.getAllByText("Included Service 1")[0]).toBeInTheDocument();
    expect(screen.getAllByText("Included Service 2")[0]).toBeInTheDocument();
    expect(screen.getByText("mock description")).toBeInTheDocument();
    expect(screen.getByText(`Product code: ${productCode}`)).toBeInTheDocument();
  });
});

describe("Product Specification", () => {
  const specification = {
    details: {
      features: [{ attributes: [{name: 'att1', value: 'Yes'}, {name: 'att2', value: 'C'}] }],
    },
  };

  test("should render list of attributes", () => {
    render(<ProductDetail data={{...defaultData, ...specification}} />);
    expect(screen.getByText("att1")).toBeInTheDocument();
    expect(screen.getByText("att2")).toBeInTheDocument();
    expect(screen.getByText("Yes")).toBeInTheDocument();
    expect(screen.getByText("C")).toBeInTheDocument();
  });
});
