import { screen, render } from "@testing-library/react";
import Home, { getServerSideProps } from ".";
import mockAxios from "jest-mock-axios";
import mockProductData from "../mockData/productData.json";

const productApiPath = 'https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI'
const defaultData = {
  products: [
    {
      productId: 1,
      variantPriceRange: {
        display: {
          max: "£100.00",
        }
      },
      title: "Mock Product 1",
      image: "mockimagesrc1",
    },
    {
      productId: 2,
      variantPriceRange: {
        display: {
          max: "£200.00",
        }
      },
      title: "Mock Product 2",
      image: "mockimagesrc2",
    },
  ],
};

describe("getServerSideProps", () => {
  test("should pass product api data to server side props", async () => {
    mockAxios.get.mockResolvedValueOnce({data: mockProductData});
    const props = await getServerSideProps();
    expect(props).toEqual({ props: { data: mockProductData } });
    expect(mockAxios.get).toHaveBeenCalledTimes(1);
    expect(mockAxios.get).toHaveBeenCalledWith(productApiPath);
  });
  test("should handle api error with empty data object", async () => {
    const errorResponse = {
      data: {},
      status: 500,
      statusText: 'Server Error',
      headers: {},
      config: {},
    }
    mockAxios.get.mockResolvedValueOnce(errorResponse);
    const props = await getServerSideProps();
    expect(props).toEqual({ props: { data: {} }});
  });
});

describe("no props", () => {
  test("should render when props are undefined", () => {
    render(<Home />);
  });
});

describe("Number of Products", () => {
  test("renders a maximum of 20 products if product api returns > 20 products", () => {
    render(<Home data={mockProductData} />);
    const products = screen.getAllByTestId("product-list-item");
    expect(products.length).toEqual(20);
    expect(
      screen.queryByText("Neff N70 S515T80D1G Fully Integrated Dishwasher")
    ).not.toBeInTheDocument();
  });
  test('should display total number of Dishwashers in title', () => { 
    render(<Home data={mockProductData} />);
    expect(screen.getByText('Dishwashers (24)')).toBeInTheDocument();
  })
});

describe("Images", () => {
  test("should render images with alt text", () => {
    render(<Home data={defaultData} />);
    const imageOne = screen.getByAltText("Mock Product 1");
    expect(imageOne).toBeInTheDocument();
    expect(imageOne).toHaveAttribute("src", "mockimagesrc1");
    const imageTwo = screen.getByAltText("Mock Product 2");
    expect(imageTwo).toBeInTheDocument();
    expect(imageTwo).toHaveAttribute("src", "mockimagesrc2");
  });
});

describe("Product Information", () => {
  test("should render product title", () => {
    render(<Home data={defaultData} />);
    expect(screen.getByText("Mock Product 1")).toBeInTheDocument();
    expect(screen.getByText("Mock Product 2")).toBeInTheDocument();
  });
  test("should render product price", () => {
    render(<Home data={defaultData} />);
    expect(screen.getByText("£100.00")).toBeInTheDocument();
    expect(screen.getByText("£200.00")).toBeInTheDocument();
  });
});
