# UI Dev Technical Test - Dishwasher App

## Brief

Your task is to create a website that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

We have started the project, but we'd like you to finish it off to a production-ready standard. Bits of it may be broken.

### Product Grid

When the website is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. 

For this exercise we’d be looking at only displaying the first 20 results returned by the API.

### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details.

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Mock Data

There is mock data available for testing in the `mockData` folder.

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed Node.js using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.


## Improvements
## If I'd continued the are some things I would focus on
- Pulling the ProductDetail and Home components out into to separate files so they live under /components with their own test files - rather than being defined in the page route files (index and product-detail/[id]). 
- Split up ProductDetail into subcomponents/sections as it has quite a lot of markup for one file. It would be easier to read and alter if it was composed of serveral smaller components. 
- More error handling arround the API call for different scenarios
- Put API keys in environment variables
- I pretty much ran out of time when looking at the Carousel and Carousels are notoriously tricky to implement properly, so I added a Carousel library and used the default settings - which doesn't quite match the design but seems to work well. If I implemented a Coursel in production I would weigh up the pros and cons based on the UX and design and heavily investigate any performance and accessibility implications of including the library.
- I haven't used snapshot tests or styling based tests, something like Percy would be better for this. Likewise I didn't try and set up any end to end tests (e.g. product page to product detail) as a framework like Cypress is more suited to that. I would consider adding both of these
- PACT tests could be added for the API call the catch changes
- Storybook is maybe overkill for a project of this size but it does nicely document components and help with accessibility

## Issues
- node-fetch: I didn't know if it was part of the challenge but I had a real issue with node-fetch, it was hanging when calling the JL api server side. I tried different versions and setting headers (e.g. content-type) but I kept getting a socket hang up error (only with the JL api). Other APIs seemed to work fine - after some debugging I wasn't sure if it was the API, node-fetch or my local machine that was the problem so I tried Axios and I could call the JL API just fine - so I kept it in the project as a replacement for node-fetch. But I added it out of practicality and time considerations, not because I prefer the library itself.
- The mock Product Data didn't appear to match the api for pricing and I ended up using variantPriceRange.display.max, just to check the data was correct I replaced the mock product data with a new copy of the API response. 

## Observations/Other
- Text in the designs looked a bit more grey than mine but I've left the text color that was defined in the codebase
- Didn't add the 'read more' section for the tablet view as I didn't know what the UX would be for this
- I updated project dependencies as they contained vulnerabilites, this didn't seem to impact the build
- I had to add identity-obj-proxy for the jest.config to work
- Getting the tablet layout was a bit of guess work at different sizes but I think it's pretty close to the design
- For testing I concentrated on TDD with Testing Library and mocked out the API layer by mocking Axios
- I've kept the filename casing consistent with the original files although uppercasing React filenames also seems common practice.