import { screen, render } from "@testing-library/react";
import ProductListItem from "./product-list-item";

test('should render when props are undefined', () => {
  render(<ProductListItem />)
})

test("should render image with alt text", () => {
  const props = {
    imageSrc: "MockImageSrc.com",
    imageAlt: "Product Title",
  };
  render(<ProductListItem {...props} />);
  const image = screen.getByAltText("Product Title")
  expect(image).toBeInTheDocument();
  expect(image).toHaveAttribute('src', 'MockImageSrc.com')
});

test('should render price in GBP', () => { 
  render(<ProductListItem price="£100.00" />);
  expect(screen.getByText("£100.00")).toBeInTheDocument();
})

test('should render description', () => { 
  render(<ProductListItem description="my description" />);
  expect(screen.getByText("my description")).toBeInTheDocument();
})