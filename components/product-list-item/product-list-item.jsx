import styles from "./product-list-item.module.scss";

const ProductListItem = ({ imageSrc = "", imageAlt = "", price = "", description = "" }) => {
  return (
    <div className={styles.content} data-testid="product-list-item">
      <img src={imageSrc} alt={imageAlt}/>
      <h3>{description}</h3>
      <span className={styles.price}>{`${price}`}</span>
    </div>
  );
};

export default ProductListItem;
