import { screen, render } from "@testing-library/react";
import Layout from "./layout";

test('should render when props are undefined', () => { 
  render(<Layout/>)
})

test('should render children', () => { 
  render(<Layout><h1>Child</h1></Layout>)
  expect(screen.getByText('Child')).toBeInTheDocument();
})

