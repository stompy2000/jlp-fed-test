import styles from "./product-carousel.module.scss";
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';

const ProductCarousel = ({ images = [], imageAlt = "" }) => {
  return (
    <div className={styles.productCarousel} >
      <Carousel >
        {images.map((image) => (
          <img key={image} src={`https:${image}`} alt={imageAlt}/>
        ))}
      </Carousel>
    </div>
  );
};

export default ProductCarousel;
