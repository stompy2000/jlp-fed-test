import { screen, render } from "@testing-library/react";
import ProductCarousel from "./product-carousel";

test("should render when props are undefined", () => {
  render(<ProductCarousel />);
});

test("should render image with alt text", () => {
  const props = {
    images: ["MockImageSrc1.com", "MockImageSrc2.com"],
    imageAlt: "Product Title",
  };
  render(<ProductCarousel {...props} />);
  const image = screen.getAllByAltText("Product Title")[0]
  expect(image).toBeInTheDocument();
  expect(image).toHaveAttribute('src', 'https:MockImageSrc1.com')
});
